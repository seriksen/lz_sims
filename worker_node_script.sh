#!/bin/bash

# Save machine OS for debugging
cat /etc/os-release
echo

# Save to ganga stdout
export PARTICLE="${PARTICLE}"
export SEED="${SEED}"
export OUTPUT_FILENAME="${OUTPUT_FILENAME}"
export OUTPUT_DIR="./"
export NBEAMON="${NBEAMON}"
export MACRO="${MACRO}"
export PYTHON_SCRIPT="${PYTHON_SCRIPT}"
export HDFS_NPY_OUTPUT_DIR="${HDFS_NPY_OUTPUT_DIR}"
export HDFS_LZAP_OUTPUT_DIR="${HDFS_LZAP_OUTPUT_DIR}"
export HDFS_BACCARAT_OUTPUT_DIR="${HDFS_BACCARAT_OUTPUT_DIR}"
export THIS_BACCARAT_VERSION="${THIS_BACCARAT_VERSION}"
export THIS_DER_VERSION="${THIS_DER_VERSION}"
export THIS_LZAP_VERSION="${THIS_LZAP_VERSION}"
export LZAP_STEERING_FILE="${LZAP_STEERING_FILE}"
export RUN_VERBOSE_ANALYSIS="${RUN_VERBOSE_ANALYSIS}"
export BACCARAT_ROOT_OUTPUT="${BACCARAT_ROOT_OUTPUT}"


echo "Environment Variables"
echo "export PARTICLE=${PARTICLE}"
echo "export NBEAMON=${NBEAMON}"
echo "export OUTPUT_FILENAME=${OUTPUT_FILENAME}"
echo "export OUTPUT_DIR=./"
echo "export MACRO=${MACRO}"
echo "export PYTHON_SCRIPT=${PYTHON_SCRIPT}"
echo "export SEED=${SEED}"
echo "export HDFS_NPY_OUTPUT_DIR=${HDFS_NPY_OUTPUT_DIR}"
echo "export HDFS_LZAP_OUTPUT_DIR=${HDFS_LZAP_OUTPUT_DIR}"
echo "export HDFS_BACCARAT_OUTPUT_DIR=${HDFS_BACCARAT_OUTPUT_DIR}"
echo "export THIS_BACCARAT_VERSION=${THIS_BACCARAT_VERSION}"
echo "export THIS_DER_VERSION=${THIS_DER_VERSION}"
echo "export THIS_LZAP_VERSION=${THIS_LZAP_VERSION}"
echo "export LZAP_STEERING_FILE=${LZAP_STEERING_FILE}"
echo "export RUN_VERBOSE_ANALYSIS=${RUN_VERBOSE_ANALYSIS}"
echo "export BACCARAT_ROOT_OUTPUT=${BACCARAT_ROOT_OUTPUT}"
echo

# Making output directories
echo "Working Dir: ${PWD}"
echo "ls of PWD"
ls
echo

# BACCARAT
echo "Running BACCARAT"
source /cvmfs/lz.opensciencegrid.org/BACCARAT/release-${THIS_BACCARAT_VERSION}/x86_64-centos7-gcc8-opt/setup.sh
BACCARATExecutable $MACRO > verbose_${SEED}.log
echo "BACCARAT FINISHED"
echo "ls of PWD"
ls
echo 

if [ "${RUN_VERBOSE_ANALYSIS}" = 1 ]
then
echo "Running VERBOSE ANALYSIS"
hdfs dfs -mkdir -p ${HDFS_NPY_OUTPUT_DIR}
python ${PYTHON_SCRIPT} -f verbose_${SEED}.log -o ${PARTICLE}_seed${SEED}
# TODO add ls | grep -i .npy | wc
echo "Copying to numpy to ${HDFS_NPY_OUTPUT_DIR}"
hdfs dfs -copyFromLocal *.npy ${HDFS_NPY_OUTPUT_DIR}
echo "Removing numpy arrays"
rm *.npy
fi

echo "Removing logs"
rm *.log
echo "ls of PWD"
ls
echo


if [ "${BACCARAT_ROOT_OUTPUT}" = 1 ]
then
# Transfer to hdfs
hdfs dfs -mkdir -p ${HDFS_BACCARAT_OUTPUT_DIR}

echo "Transferring LZap output to ${HDFS_BACCARAT_OUTPUT_DIR}"
hdfs dfs -copyFromLocal *root_${SEED}.root ${HDFS_BACCARAT_OUTPUT_DIR}

echo "Removing final bits"
rm *.root
rm *.log
echo "Complete!"s

else



echo "Running BaccRootConverter"
BaccRootConverter ${OUTPUT_DIR}${OUTPUT_FILENAME}${SEED}.bin > root_converter_${SEED}.log
echo "Running BaccMCTruth"
BaccMCTruth ${OUTPUT_DIR}${OUTPUT_FILENAME}${SEED}.root > baccmctruth_${SEED}.log
echo "ls of PWD"
ls 
echo
echo "Removing logging"
rm *.log
rm *.bin
rm ${OUTPUT_DIR}${OUTPUT_FILENAME}${SEED}.root
echo "ls of PWD"
ls
echo


# DER
source /cvmfs/lz.opensciencegrid.org/DER/release-${THIS_DER_VERSION}/x86_64-centos7-gcc8-opt/setup.sh
echo "Running DER"
echo "DER --RandomNumberSeed ${SEED} --fileSeqNum ${SEED} --AftPls false --GenerateDarkCounts false --allPhotonMCTruth true --PMTParamsPath /cvmfs/lz.opensciencegrid.org/DER/PMTParameters/PMTParameters_31Oct21.csv ${OUTPUT_FILENAME}${SEED}_mctruth.root > der${SEED}.log"
DER --RandomNumberSeed ${SEED} --fileSeqNum ${SEED} --AftPls false --GenerateDarkCounts false --allPhotonMCTruth true --PMTParamsPath /cvmfs/lz.opensciencegrid.org/DER/PMTParameters/PMTParameters_31Oct21.csv ${OUTPUT_FILENAME}${SEED}_mctruth.root > der${SEED}.log
echo
echo "ls of PWD"
ls
echo

# LZap
source /cvmfs/lz.opensciencegrid.org/LZap/release-${THIS_LZAP_VERSION}/x86_64-centos7-gcc8-opt/setup.sh
export DER_SEED=$(printf "%06d" ${SEED})
export LZAP_INPUT_FILES=$(echo lz*_${DER_SEED}_raw.root)
export LZAP_OUTPUT_FILE=LZAP_SEED${SEED}.root
echo
echo "Running LZap with;"
echo "LZAP_INPUT_FILES=${LZAP_INPUT_FILES}"
echo "LZAP_OUTPUT_FILES=${LZAP_OUTPUT_FILE}"
echo "lzap ${LZAP_STEERING_FILE} >${OUTPUT_DIR}lzap${SEED}_log.log"
lzap ${LZAP_STEERING_FILE} >${OUTPUT_DIR}lzap${SEED}_log.log

echo "ls of PWD"
ls
echo


# Transfer to hdfs
hdfs dfs -mkdir -p ${HDFS_LZAP_OUTPUT_DIR}

echo "Transferring LZap output to ${HDFS_LZAP_OUTPUT_DIR}"
hdfs dfs -copyFromLocal ${LZAP_OUTPUT_FILE} ${HDFS_LZAP_OUTPUT_DIR}

echo "Removing final bits"
rm *.root
rm *.log
echo "Complete!"s

fi
